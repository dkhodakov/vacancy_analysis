Vacancy analysis
================
Web crawler and data analizer with web interface. 
Currently used to gather information about programmer vacancies.

How to use it:

1. Create data folders: **mkdir -p /opt/vacan/data; mkdir -p /opt/vacan/common;**
2. Download statistics info to vacancy database: **./vacan_proc -c**
3. Run web interface: **./vacan_web**
4. Open http://localhost:9999 in your favorite browser and see results.

