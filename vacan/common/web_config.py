#!/usr/bin/env python3

""" This file contains main configuration of Data Analyzer Chart.
"""

PORT = 9999
NUMBER_OF_BINS = 20
STAT_DB = '/opt/vacan/common/stat.db'
